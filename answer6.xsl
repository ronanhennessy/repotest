<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <html>
            <body>
                <h1>Female students in XML module</h1>
                <table border="1">
                    <!-- Tried a different colour (grey) -->
                    <tr bgcolor="#C0C0C0">
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                    <xsl:for-each select="//module[@ModuleName='XML']/students/student[@gender='F']">
                        
                        <tr>
                            <td>
                                <xsl:value-of select="firstname"/>
                                
                            </td>
                            <td><xsl:value-of select="lastname"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>
