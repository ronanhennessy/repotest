<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays all programmes in the Computer Science
                    department.</p>

                <h2>Computer Science department</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Programme title</th>
                        <th>Host department</th>
                        <th>NFQ</th>
                        <th>Delivery</th>
                        <th>Semesters</th>
                        <th>Credits</th>
                        <th>Modules</th>
                    </tr>

                    <!-- Selects departments with the name Computer Science -->
                    <xsl:for-each select="//department/name[text() = 'Computer science']">

                        <!-- Selects each host programme -->
                        <xsl:for-each select="..//programme">

                            <!-- Prints values in table format -->
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/>

                                </td>
                                <td>
                                    <xsl:value-of select="hostdepartment"/>
                                </td>
                                <td>
                                    <xsl:value-of select="NFQ"/>

                                </td>
                                <td>
                                    <xsl:value-of select="delivery"/>

                                </td>
                                <td>
                                    <xsl:value-of select="semesters"/>

                                </td>
                                <td>
                                    <xsl:value-of select="credits"/>

                                </td>
                                <!-- Selects each module name if there are more than one -->
                                <xsl:for-each select="module">
                                    <td>
                                        <xsl:value-of select="@name"/>
                                    </td>
                                </xsl:for-each>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
