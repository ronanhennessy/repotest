<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays modules with an NFQ level of 9 or greater.</p>

                <h2>NFQ level of 9 or greater</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Module name</th>
                        <th>NFQ level</th>

                    </tr>

                    <!-- Selects all modules with NFQ level greater than 8 -->
                    <xsl:for-each select="//module[@NFQ > 8]">

                        <!-- prints them in a table -->
                        <tr>
                            <td>
                                <xsl:value-of select="@name"/>

                            </td>
                            <td>
                                <xsl:value-of select="@NFQ"/>
                            </td>

                        </tr>

                    </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
