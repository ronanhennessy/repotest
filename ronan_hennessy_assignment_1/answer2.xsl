<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays administrators for the Faculty, School and
                    Department.</p>

                <h2>Administrators</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Level</th>
                    </tr>

                    <!-- Selects all faculties and the child administrators -->
                    <xsl:for-each select="/college//faculty">
                        <xsl:for-each select="child::administrator">

                            <!-- Prints details -->
                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                                <td>
                                    <xsl:value-of select="../@name"/>

                                </td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>

                    <!-- Selects all schools and the child administrators -->
                    <xsl:for-each select="/college//faculty//school">
                        <xsl:for-each select="child::administrator">
                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                                <td>
                                    <xsl:value-of select="../name"/>

                                </td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>

                    <!-- Selects all departments and the child administrators -->
                    <xsl:for-each select="/college//faculty//school//department">
                        <xsl:for-each select="child::administrator">
                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                                <td>
                                    <xsl:value-of select="../name"/>

                                </td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
