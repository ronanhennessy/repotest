<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <html>
            <body>
                <p>The following tables display the heads of the Faculty, School and Department.</p>
                
                <h2>Heads of Faculty</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Faculty</th>
                    </tr>
                    
                    <!-- Selects the faculty and prints the name -->
                    <xsl:for-each select="/college/faculty">
                        
                        <tr>
                            <td>
                                <xsl:value-of select="head/firstname"/>
                                
                            </td>
                            <td><xsl:value-of select="head/lastname"/></td>
                            <td>
                                <xsl:value-of select="@name"/>
                                
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
                <h2>Heads of School</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>School</th>
                    </tr>
                    <xsl:for-each select="/college/faculty/school">
                    
                        
                        <tr>
                            <td>
                                <xsl:value-of select="head/firstname"/>
                                
                            </td>
                            <td><xsl:value-of select="head/lastname"/></td>
                            
                            <td><xsl:value-of select="name"/></td>
                        </tr>
                    
                    </xsl:for-each>
                </table>
                <h2>Heads of Department</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Department</th>
                    </tr>
                    <xsl:for-each select="/college/faculty/school/department">
                        
                        <tr>
                            <td>
                                <xsl:value-of select="head/firstname"/>
                                
                            </td>
                            <td><xsl:value-of select="head/lastname"/></td>
                            <td><xsl:value-of select="name"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>