<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays details from all modules attended by student Deborah
                    Murphy.</p>

                <h2>Module detials</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Module name</th>
                        <th>CRN</th>
                        <th>NFQ</th>
                        <th>Credits</th>
                        <th>Co-ordinator</th>
                        <th>Author</th>

                    </tr>
                    <!-- Selects all modules -->
                    <xsl:for-each select="//module">

                        <!-- Tests for students with name Deborah Murphy -->
                        <xsl:if test="students/student/firstname = 'Deborah'">
                            <xsl:if test="students/student/lastname = 'Murphy'">

                                <!-- This table prints the student details -->
                                <tr>
                                    <td>
                                        <xsl:value-of select="@name"/>

                                    </td>

                                    <td>
                                        <xsl:value-of select="@CRN"/>

                                    </td>

                                    <td>
                                        <xsl:value-of select="@NFQ"/>

                                    </td>

                                    <td>
                                        <xsl:value-of select="@credits"/>

                                    </td>

                                    <td>
                                        <xsl:value-of select="@co-ordinator"/>

                                    </td>

                                    <td>
                                        <xsl:value-of select="@author"/>

                                    </td>

                                </tr>
                            </xsl:if>
                        </xsl:if>

                    </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
