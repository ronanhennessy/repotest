<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays student details for all students currently withdrawn
                    from the programme Diploma in Information Design and Development.</p>

                <h1>Students withdrawn from the Diploma in Information Design and Development </h1>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Student ID</th>
                        <th>Gender</th>
                        <th>Home</th>
                        <th>Telephone</th>
                        <th>Status</th>
                    </tr>

                    <!-- Selects all students elements in the programme Diploma in Information Design and Development  -->
                    <xsl:for-each
                        select="//programme/title[text() = 'Diploma in Information Design and Development']/..//module//students">

                        <!-- Selects each student elements with a 'withdrawn' attribute -->
                        <xsl:for-each select="student[@status = 'withdrawn']">


                            <!-- This prints out a table of the student details -->
                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>

                                <td>
                                    <xsl:value-of select="lastname"/>

                                </td>

                                <td>
                                    <xsl:value-of select="@studentID"/>

                                </td>

                                <td>
                                    <xsl:value-of select="@gender"/>

                                </td>

                                <td>
                                    <xsl:value-of select="home"/>

                                </td>

                                <td>
                                    <xsl:value-of select="telephone"/>

                                </td>
                                <td>
                                    <xsl:value-of select="@status"/>

                                </td>
                            </tr>

                        </xsl:for-each>



                    </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
