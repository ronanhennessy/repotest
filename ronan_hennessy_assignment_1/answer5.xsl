<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays all registered students in the XML in Technical
                    Communications module.</p>

                <h2>Registered students</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>

                    </tr>
                    <!-- selects all modules with the name attribute XML in Technical Communications -->
                    <xsl:for-each select="//module[@name = 'XML in Technical Communications']">

                        <!-- selects all students with status attribute 'registered -->
                        <xsl:for-each select="students//student[@status = 'registered']">

                            <!-- Prints their name in a table -->
                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>

                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
