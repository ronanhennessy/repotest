<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following table displays all postgraduate programmes in the Compter Science
                    department</p>

                <h2>Postgraduate programmes</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Programme name</th>
                        <th>Level</th>

                    </tr>

                    <!-- Selects departments with name Computer Science -->
                    <xsl:for-each select="//department/name[text() = 'Computer science']">

                        <!-- At one level up, selects programmes with a postgraduate attrubute -->
                        <xsl:for-each select="..//programme[@level = 'postgraduate']">

                            <!-- Prints details in a table -->
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/>

                                </td>
                                <td>
                                    <xsl:value-of select="@level"/>
                                </td>

                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
