<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <p>The following list displays all modules lectured by Kevin Coleman.</p>

                <h2>Modules lectured by Kevin Coleman</h2>

                <!-- Begins an unordered list element -->
                <ul>

                    <!-- Selects all lecturers -->
                    <xsl:for-each select="//module//lecturer">

                        <!-- Tests for the lecturer name and prints the module if name matches -->
                        <xsl:if test="firstname = 'Kevin'">
                            <xsl:if test="lastname = 'Coleman'">
                                <li>
                                    <xsl:value-of select="../../module/@name"/>
                                </li>
                            </xsl:if>
                        </xsl:if>
                    </xsl:for-each>
                </ul>


                <!-- The below code displays the same output but in a table -->
                <!-- <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Module name</th>
                        
                        
                    </tr>
                    <xsl:for-each select="//module//lecturer">
                        <xsl:if test="firstname='Kevin'">
                            <xsl:if test="lastname='Coleman'">
                            
                            <tr>
                                <td>
                                    <xsl:value-of select="../../module/@name"/>
                                    
                                </td>
                                
                                
                            </tr>
                        </xsl:if>
                        </xsl:if>
                        </xsl:for-each>
                    
                </table> -->

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
