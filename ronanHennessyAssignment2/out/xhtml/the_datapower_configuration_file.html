<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="DC.Type" content="concept" />
<meta name="DC.Title" content="The DataPower configuration file" />
<meta name="DC.subject" content="configuration file, DataPower" />
<meta name="keywords" content="configuration file, DataPower" />
<meta name="DC.Relation" scheme="URI" content="enabling_data_collection.html" />
<meta name="DC.Relation" scheme="URI" content="enabling_data_collection.html" />
<meta name="DC.Relation" scheme="URI" content="enabling_data_collection_using_the_data_collector_configuration_utility.html" />
<meta name="prodname" content="Datapower" />
<meta name="brand" content="IBM" />
<meta name="DC.Creator" content="Ronan Hennessy" />
<meta name="copyright" content="© Copyright IBM Corporation. U.S. Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp. 2017" type="primary" />
<meta name="DC.Rights.Owner" content="© Copyright IBM Corporation. U.S. Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp. 2017" type="primary" />
<meta name="DC.Date.Created" content="18/11/2017" />
<meta name="DC.Date.Modified" content="19/11/2017" />
<meta name="DC.Format" content="XHTML" />
<meta name="DC.Identifier" content="the_datapower_configuration_file" />
<link rel="stylesheet" type="text/css" href="commonltr.css" />
<title>The DataPower configuration file</title>
</head>
<body id="the_datapower_configuration_file">

 <h1 class="title topictitle1" id="ariaid-title1">The DataPower configuration file</h1>

 
 <div class="body conbody">
  <p class="p">For the DataPower data collector, the Data Collector Configuration utility and the KD4configDC
   command manipulate the contents of a special DataPower configuration file by adding sections to
   the file when new DataPower monitoring is enabled, and removing sections from the file when
   monitoring is disabled. Each invocation of the Data Collector Configuration utility or the
    <strong class="ph b">KD4configDC</strong> command adds, updates, or removes one section of the DataPower configuration
   file. Each section of the DataPower configuration file might be associated with its own data
   group, or it might be part of a larger data group to which other sections of the configuration
   file also belong.</p>

  <p class="p">The DataPower data collector uses this configuration file to identify the DataPower SOA
   appliances that are to be monitored and to specify all of the information that is needed to
   communicate with those appliances. Typical information that is stored for each connection
   includes host name and port, user ID and password, domains to monitor, and polling interval.</p>

  <p class="p"><span class="ph">The <span class="ph filepath">ITCAM4SOA_Home/KD4/</span>config directory on Windows
    systems and is called KD4.dpdcConfig.properties.</span> This file is maintained separately from
   the existing KD4.dc.properties configuration file. This is a sample DataPower configuration
   file:</p>

  <pre class="pre codeblock"><code># Sample DataPower data collector configuration file
DataPower.count=3
#
DataPower.host.1=dpbox1
DataPower.port.1=5550
DataPower.path.1=/
DataPower.poll.1=60
DataPower.user.1=admin
DataPower.encpswd.1=#$%*&amp;
DataPower.domainlist.1=default,testdom1
DataPower.displaygroup.1=dpbox1
DataPower.subExpire.1=15
DataPower.maxrecords.1=1000

#
DataPower.host.2=dpbox2
DataPower.port.2=5550
DataPower.path.2=/
DataPower.poll.2=30
DataPower.user.2=user1
DataPower.encpswd.2=&amp;*%$#
DataPower.domainlist.2=userdom1,userdom2,userdom3
DataPower.displaygroup.2=user_doms
DataPower.subExpire.2=15
DataPower.maxrecords.2=1000
#
DataPower.host.3=dpbox2
DataPower.port.3=5550
DataPower.path.3=/
DataPower.poll.3=30
DataPower.user.3=admin
DataPower.encpswd.3=*%$#&amp;
DataPower.displaygroup.3=all_doms
DataPower.subExpire.3=15
DataPower.maxrecords.3=1000</code></pre>
  <p class="p">In the example, there are three sections in the configuration file. The properties in each of
   the three sections provide all of the information that is needed to establish and manage a single
   connection or session with each DataPower SOA appliance.</p>

  <p class="p">Change the information in this configuration file using either the Data Collector Configuration
   utility or the <strong class="ph b">KD4configDC</strong> command. You can only modify the parameters that are set when
   you first run the Data Collector Configuration utility or when you first issued the
    <strong class="ph b">KD4configDC</strong> command. To set additional parameters, you must manually add them to the
   configuration file.</p>

  <p class="p">Using various combinations of parameters in the Data Collector Configuration utility input
   pages or in the <strong class="ph b">KD4configDC</strong> command, you can achieve different monitoring configurations
   to separate or aggregate data among domains and appliances. See “Considerations for enabling data
   collection for DataPower monitoring” on page 28 for more information.</p>

  <p class="p">Before you configure your DataPower environment for data collection, consult with your local
   systems management planners to understand which domains on which DataPower SOA appliances are to
   be monitored and how the data from these domains and appliances should be separated or aggregated
   for display in the Tivoli Enterprise Portal.</p>

  <p class="p">To set the DataPower.maxrecords property to an optimal value, it is useful to determine the
   number of transactions that are processed by each of the configured domains. The
   DataPower.maxrecords property must be set in line with the expected traffic levels of each
   configured domain. For more information about setting the transaction rate, see “Optimizing
   performance” on page 40.</p>

  <p class="p"><strong class="ph b">Restriction:</strong> In an upgrade scenario, to set the maximum number of records for an
   existing display group, you must add the DataPower.maxrecords parameter manually to the section
   in the KD4.dpdcConfig.properties file that configures the display group.</p>

  <div class="section" id="the_datapower_configuration_file__section_fzq_2f4_xbb"><h2 class="title sectiontitle">Additional properties</h2>
   
   <p class="p">Beginning with ITCAM for SOA version 7.2 Fix Pack 1, you can optionally add the following
    properties manually to the KD4.dpdcConfig.properties file:</p>

   <dl class="dl">
    
     <dt class="dt dlterm">DataPower.dimension.domain</dt>

     <dd class="dd">
      <p class="p">Adds the DataPower domain attribute to the Service Inventory_610, the Service Inventory
       Requester Identity_610, and the Fault Log_610 tables. The domain attribute is added to the
       Application Server Cluster Name column in each table</p>

     </dd>

    
    
     <dt class="dt dlterm">DataPower.alias</dt>

     <dd class="dd">Assigns an alias to the DataPower host. If this property is set, the alias is added to the
      Application Server Cell Name column in the Service Inventory_610, the Service Inventory
      Requester Identity_610, and the Fault Log_610 tables. Alternatively, you can use the
      DataPower.displaygroup property to create an alias for the DataPower host.</dd>

    
    
     <dt class="dt dlterm">DataPower.multihost.group</dt>

     <dd class="dd">
      <p class="p">The DataPower.displaygroup property defines the name to display for the DataPower appliance
       node in the Navigator view in the Tivoli Enterprise Portal. When the property is set, data
       from all DataPower appliances that are associated with the display group is aggregated for
       the display group. The data is available from the single node in the Navigator view.</p>

      <p class="p">The display group might be associated with different domains on the single DataPower
       appliance. In the Service Inventory_610, the Service Inventory Requester Identity_610, and
       the Fault Log_610 tables, the Application Server Node Name attribute displays the name of
       this appliance. Alternatively, the display group might be associated with multiple
       appliances. The Application Server Node Name attribute displays the name of only one of the
       appliances.</p>

      <p class="p">By setting the DataPower.multihost.group property to true, you indicate that there are
       multiple appliances associated with a display group. The Application Server Node Name
       attribute is not populated. By setting the property to false, you indicate that the display
       group is associated with a single DataPower appliance. The name of the DataPower appliance is
       added to the Application Server Node Name column. If the DataPower.multihost.group property
       is not specified in the configuration file, the property is set to false.</p>

     </dd>

    
   </dl>

  </div>

 </div>

<div class="related-links">
<div class="familylinks">
<div class="parentlink"><strong>Parent topic:</strong> <a class="link" href="enabling_data_collection.html" title="This section describes how to configure the DataPower environment for data collection.">Enabling data collection</a></div>
<div class="previouslink"><strong>Previous topic:</strong> <a class="link" href="enabling_data_collection.html" title="This section describes how to configure the DataPower environment for data collection.">Enabling data collection</a></div>
<div class="nextlink"><strong>Next topic:</strong> <a class="link" href="enabling_data_collection_using_the_data_collector_configuration_utility.html">Enabling data collection using the Data Collector Configuration utility</a></div>
</div>
</div></body>
</html>