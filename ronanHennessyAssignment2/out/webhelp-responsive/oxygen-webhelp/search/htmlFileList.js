//List of files which are indexed.
fl = new Array();
fl["0"]= "configuring_a_user_account_on_the_datapower_soa_appliance.html";
fl["1"]= "configuring_data_collection__datapower_soa_appliance.html";
fl["2"]= "configuring_the_datapower_soa_appliance_for_monitoring.html";
fl["3"]= "configuring_the_xml_management_interface.html";
fl["4"]= "deployment_steps.html";
fl["5"]= "disabling_data_collection.html";
fl["6"]= "disabling_data_collection_using_the_data_collector_configuration_utility.html";
fl["7"]= "enabling_data_collection.html";
fl["8"]= "enabling_data_collection_using_the_data_collector_configuration_utility.html";
fl["9"]= "enabling_data_collection_using_the_kd4configdc_command.html";
fl["10"]= "planning_for_deployment.html";
fl["11"]= "specifying_kd4configdc_parameters.html";
fl["12"]= "the_datapower_configuration_file.html";
fl["13"]= "the_datapower_data_collector_as_a_proxy.html";
fl["14"]= "unconfiguration_steps.html";
fl["15"]= "upgrading_the_data_collector.html";
var doStem = false;searchLoaded = true;