<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Heads of all departments</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                    <xsl:for-each select="//head">

                        <!-- The if statement removes students that have the same first name. My attempt at removing duplicate names. -->
                        <xsl:if
                            test="not(preceding::head[firstname/text() = current()/firstname/text()])">

                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
