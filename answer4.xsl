<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Computing department head</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>

                    <!-- Selects department head with attribute "Department of computing" -->
                    <xsl:for-each select="//department[@name = 'Depatment of Computing']/head">
                        <xsl:if
                            test="not(preceding::head[firstname/text() = current()/firstname/text()])">

                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
