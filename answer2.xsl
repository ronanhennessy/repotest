<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Msc Info Dev students</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                    <xsl:for-each
                        select="//programme[@programmeCode = 'P0001']/module/students/student">

                        <xsl:if
                            test="not(preceding::student[firstname/text() = current()/firstname/text()])">

                            <tr>
                                <td>
                                    <xsl:value-of select="firstname"/>

                                </td>
                                <td>
                                    <xsl:value-of select="lastname"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
